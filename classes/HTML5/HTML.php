<?php

defined('SYSPATH') or die('No direct script access.');

class HTML5_HTML extends Kohana_HTML
{

    /**
     * Encodes string to ascii equivalent.
     *
     * @param string String to encode
     * @return string
     */
    public static function encodeToAscii($string)
    {
        $ascii = '';
        $stringLength = strlen($string);
        for ($index = 0; $index < $stringLength; ++$index)
        {
            $ascii .= '&#'.ord($string[$index]).';';
        }

        return $ascii;
    }

    /**
     * Creates an email (mailto:) anchor. Note that the title is not escaped,
     * to allow HTML elements within links (images, etc).
     *
     *     echo HTML::mailto($address);
     *
     * @param   string  email address to send to
     * @param   string  link text
     * @param   array   HTML anchor attributes
     * @return  string
     * @uses    HTML::attributes
     */
    public static function mailto($email, $title = NULL,
        array $attributes = NULL)
    {
        $email = HTML::encodeToAscii($email);

        if ($title === NULL)
        {
            // Use the email address as the title
            $title = $email;
        }

        return '<a href=&#109;&#097;&#105;&#108;&#116;&#111;&#058;'.$email.HTML::attributes($attributes).'>'.$title.'</a>';
    }

    /**
     * Creates a style sheet link element.
     *
     *     echo HTML::style('media/css/screen.css');
     *
     * @param   string   file name
     * @param   array    default attributes
     * @param   mixed    protocol to pass to URL::base()
     * @param   boolean  include the index page
     * @return  string
     * @uses    URL::base
     * @uses    HTML::attributes
     */
    public static function style($file, array $attributes = NULL,
        $protocol = NULL, $index = FALSE)
    {
        if (strpos($file, '://') === FALSE)
        {
            // Add the base URL
            $file = URL::base($protocol, $index).$file;
        }

        // Set the stylesheet link
        $attributes['href'] = $file;

        // Set the stylesheet rel
        $attributes['rel'] = 'stylesheet';

        return '<link'.HTML::attributes($attributes).'>';
    }

    /**
     * Creates a script link.
     *
     *     echo HTML::script('media/js/jquery.min.js');
     *
     * @param   string   file name
     * @param   array    default attributes
     * @param   mixed    protocol to pass to URL::base()
     * @param   boolean  include the index page
     * @return  string
     * @uses    URL::base
     * @uses    HTML::attributes
     */
    public static function script($file, array $attributes = NULL,
        $protocol = NULL, $index = FALSE)
    {
        if (strpos($file, '://') === FALSE)
        {
            // Add the base URL
            $file = URL::base($protocol, $index).$file;
        }

        // Set the script link
        $attributes['src'] = $file;

        return '<script'.HTML::attributes($attributes).'></script>';
    }

    /**
     * Added support for HTML syntax
     *
     * Compiles an array of HTML attributes into an attribute string.
     * Attributes will be sorted using HTML::$attribute_order for consistency.
     *
     *     echo '<div'.HTML::attributes($attrs).'>'.$content.'</div>';
     *
     * @param   array   attribute list
     * @return  string
     */
    public static function attributes(array $attributes = NULL)
    {
        if (empty($attributes)) return '';

        $compiled = '';
        foreach ($attributes as $key=> $value)
        {
            if (empty($key))
            {
                $compiled .= ' '.$value;
            }
            elseif (empty($value))
            {
                $compiled .= ' '.$key;
            }
            else
            {
                $compiled .= ' '.$key.'='.
                    (strpos($value, ' ') === FALSE ? HTML::chars($value) : '"'.HTML::chars($value).'"');
            }
        }

        return $compiled;
    }

}

// End html
